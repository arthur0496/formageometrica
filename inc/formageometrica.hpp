#ifndef FORMAGEOMETRICA_HPP
#define FORMAGEOMETRICA_HPP

class FormaGeometrica {

private: 
   int lados;
   float area;
   float perimetro;
   float base;
   float altura;	
public:
   FormaGeometrica();
   FormaGeometrica(int lados, float base, float altura);
   //~FormaGeometrica();
   void setBase(float base);
   float getBase();
   void setAltura(float altura);
   float getAltura();
   void setLados(int lados);
   int getLados();
   float getArea();
   float getPerimetro();

   virtual void calculaArea() = 0;
   virtual void calculaPerimetro() = 0;

protected:
   void setArea(float area);
   void setPerimetro(float perimetro);

};


#endif
