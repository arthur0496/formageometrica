#include <iostream>
#include <math.h>
#include "triangulo.hpp"

using namespace std;

Triangulo::Triangulo(){
    setBase(10);
    setAltura(20);
    setLados(3);
}

Triangulo::Triangulo(float base, float altura){
  cout << "#Contrutor do Triangulo" << endl;
    setBase(base);
    setAltura(altura);
    setLados(3);
}

// Triangulo::~Triangulo(){
//   cout << "Destrutor: Triangulo" << endl;
// }

void Triangulo::calculaArea(){
  std::cout << "**Calculo de Area do Triangulo " << std::endl;
    setArea(getBase() * getAltura() / 2);
}

void Triangulo::calculaPerimetro(){
  std::cout << "**Calculo de Perímetro do Triangulo " << std::endl;  
  float h;
  h = sqrt(powf(getBase(),2) + powf(getAltura(),2));
    setPerimetro( getBase() + getAltura() + h);
}
